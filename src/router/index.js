import Vue from "vue";
import VueRouter from "vue-router";
import EventCreate from "@/views/EventCreate.vue";
import EventShow from "@/views/EventShow.vue";
import EventList from "@/views/EventList.vue";
import NotFoundPage from '@/views/NotFoundPage.vue';
import NProgress from 'nprogress';
import store from '@/store';
import NetworkIssue from '@/components/NetworkIssue.vue';


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "event-list",
    component: EventList,
    props: true,
  },
  {
    path: "/event/create",
    name: "event-create",
    component: EventCreate,
    props: true,
  },
  {
    path: "/event/:id",
    name: "event-show",
    props: true,
    component: EventShow,
    beforeEnter(routeTo, routeFrom, next) {
      store.dispatch('event/fetchEvent', routeTo.params.id).then(event => {
        routeTo.params.event = event;
        next();
      })
      .catch(error => {
        if (error.response && error.response.status == 404) {
          next({name: '404-not-found', params: { resource: 'event' }});
        }
        else {
          next({name: 'network-issue'});
        }
      });
    },
  },
  {
    path: "/404",
    name: "404-not-found",
    component: NotFoundPage,
    props: true,
  },
  {
    path: "/network-issue",
    name: "network-issue",
    component: NetworkIssue,
    props: true,
  },
  {
    path: "*",
    redirect: { name: '404-not-found', params: { resource: 'page' } },
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

router.beforeEach((routeTo, routeFrom, next) => {
  NProgress.start();
  next();
});

router.afterEach(() => {
  NProgress.done();
});


export default router;
