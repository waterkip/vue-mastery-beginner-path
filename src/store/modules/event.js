import EventService from '@/services/EventService.js'

export const namespaced = true;

export const state = {
  events: [],
  hasNextEventPage: true,
  event: {},
  perPage: 3,
};

export const mutations = {
  ADD_EVENT(state, event) {
    state.events.push(event)
  },
  SET_EVENTS(state, events) {
    state.events = events
  },
  HAS_NEXT_EVENT(state, { hasNextEvent }) {
    state.hasNextEventPage = hasNextEvent
  },
  SET_CURRENT_EVENT(state, event) {
    state.event = event;
  }
}

export const actions = {
  createEvent({ commit, dispatch }, event) {
    return EventService.postEvent(event).then(() => {
      commit('ADD_EVENT', event);
      const notification = {
        type: 'success',
        message: "Successfully added event" + event.title,
      }
      dispatch('notification/add', notification, { root: true });
    }).catch(error => {
      const notification = {
        type: 'error',
        message: "Error adding event: " + error.response,
      }
      dispatch('notification/add', notification, { root: true });
      throw error;
    });
  },
  hasNextEvents({commit, state}, { page }) {
    const maxPages = state.perPage * page;

    EventService.getEvents(1, maxPages + 1)
      .then(response => {
        commit('HAS_NEXT_EVENT',
          {
            hasNextEvent: response.data.length !== 0,
            maxPage: maxPages
          }
        );
      }).
      catch(error => {
        const notification = {
          type: 'error',
          message: "Error getting events" + error.response,
        }
        dispatch('notification/add', notification, { root: true });
        throw(error);
    });

  },
  fetchEvents({ commit, dispatch, state }, { page }) {
    return EventService.getEvents(state.perPage, page)
      .then(response => {
        commit('SET_EVENTS', response.data);
        const maxPage = state.perPage * page;
        if (response.data.length < state.perPage) {
          commit('HAS_NEXT_EVENT', { hasNextEvent: false, page });
        }
        else {
          dispatch('hasNextEvents', {page});
        }
      }).
      catch(error => {
        const notification = {
          type: 'error',
          message: "Error fetchting events" + error.response,
        }
        dispatch('notification/add', notification, { root: true });
        throw(error);
    });
  },
  fetchEvent({ commit, getters, dispatch, state }, id) {
    if (id == state.event.id) {
      return state.event;
    }

    const event = getters.getEventById(id)
    if (event) {
        commit('SET_CURRENT_EVENT', event);
        return event;
    }
    return EventService.getEvent(id)
      .then(response => {
        commit('SET_CURRENT_EVENT', response.data);
        return response.data;
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message: `Error fetchting event with id ${id}`
        }
        dispatch('notification/add', notification, { root: true });
        throw(error);
    });
  },
};

export const getters = {
  getEventById: state => id => {
    return state.events.find(event => event.id === id);
  },
  hasNextEventPage: state => {
    return state.hasNextEventPage;
  }
}
