import axios from 'axios';

const apiClient = axios.create({
    baseURL: 'http://localhost:3000',
    withCredentials: false,
    headers: {
        accept: 'application/json',
        'content-type': 'application/json',
    },
    timeout: 2500,
});

export default {
    getEvents(perPage, page) {
        const qs = `_limit=${perPage}&_page=${page}`;
        return apiClient.get(`/events?${qs}`);
    },
    getEvent(id) {
        return apiClient.get(`/events/${id}`);
    },
    postEvent(event) {
        return apiClient.post('/events', event);
    }
}
