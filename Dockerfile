FROM registry.gitlab.com/opndev/javascript/docker/vue:latest

COPY package.json .
RUN npm i --no-optional --no-fund

COPY . .
